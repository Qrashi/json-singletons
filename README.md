[![DeepSource](https://deepsource.io/gl/Qrashi/json-singletons.svg/?label=active+issues&show_trend=true&token=HIEoWKgcQGg_1H_GWzDwA7lj)](https://deepsource.io/gl/Qrashi/json-singletons/?ref=repository-badge)
# JsonSingletons

JsonSingletons is a small library that makes it easy to automatically deserialize and serialize various classes **using gson**.
The concept is the following:
* Any class that extends JsonSerializable can be serialized and deserialized.
* There is always only ONE single Object of your type in storage, so you don't have to worry about using wrong (old) data.

Installation:
<br> Add the repositories and dependencies to your ``pom.xml``
```xml
<repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/34255220/packages/maven</url>
</repository>
```
```xml
<dependency>
  <groupId>link.ignyte.utils</groupId>
  <artifactId>JsonSingleton</artifactId>
  <version>1.1</version>
</dependency>
```

Go to the [package registry](https://gitlab.com/Qrashi/json-singletons/-/packages) to view all available package versions (and possibly more up to date ones)

Here is how it works:

This is the object that I would like to serialize/deserialize

```java
import link.ignyte.utils.JsonSingleton.JsonPath;
import link.ignyte.utils.JsonSingleton.JsonSerializable;

class Person extends JsonSerializable {
    int age;
    String name;

    public Person() {
        // Initialize default person, REQUIRED
        age = 21;
        name = "Arda";
    }

    public String id() {
        return ("name: " + name + "; age: " + age);
    }
}
```

This is the main class:

```java
import link.ignyte.utils.JsonSingleton.GsonMode;
import link.ignyte.utils.JsonSingleton.JsonSingletons;

class Main {
    public static void main(String[] args) {
        JsonSingletons.initialize(GsonMode.ALL, "data");
        
        Person person = Person.get(Person.class);
        System.out.println(person.id());
    }
}
```

Once ``JsonSerializable.get`` is executed, the required class will get loaded and put into memory.
<br> If you execute ``JsonSerializable.get`` again with the same parameters, the one in memory will get returned.
<br> To save back to the filesystem use ``JsonSerializable.save``.

When using 
```java
JsonSingletons.initialize(GsonMode mode, String path);
```
 the avialible ``GsonModes`` are
```
* ALL (Instructs gson to serialize ALL properties)
* ALL_EXCEPT_TRANSIENT (Instructs gson to serialize ALL properties except for the properties marked with transient)
* EXPOSE_ONLY (Save all properties that have been anotated with @Expose)
```

