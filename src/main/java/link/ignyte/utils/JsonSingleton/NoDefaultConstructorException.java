package link.ignyte.utils.JsonSingleton;

/**
 * Exception thrown when there is no default to instantiate
 */
public class NoDefaultConstructorException extends RuntimeException {

    /**
     * New NoDefaultConstructorException
     * @param s Exception details to print to console
     */
    NoDefaultConstructorException(String s, Throwable cause) {
        super(s, cause);
    }

}
