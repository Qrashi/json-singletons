package link.ignyte.utils.JsonSingleton;

/**
 * Exception while writing to a file
 */
public class WriteException extends RuntimeException {
    /**
     * New WriteException
     * @param s Message to display on stdout
     */
    WriteException(String s) {
        super(s);
    }
    WriteException(String s, Throwable cause) {
        super(s, cause);
    }
}
