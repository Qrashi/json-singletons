package link.ignyte.utils.JsonSingleton;

/**
 * A class defining all serializable classes
 */
public abstract class JsonSerializable {

    /**
     * Get the instance (singleton) of a specific type
     * @param type Type of instance to get
     * @param <T> Type of instance. Must be extending JsonSerializable
     * @return Instance of T (singleton)
     */
    public static <T extends JsonSerializable> T get(Class<T> type) {
        return JsonSingletons.get(type);
    }

    /**
     * Save the instance of a type
     * @param type type of class to save
     */
    public static void save(Class<? extends JsonSerializable> type) {
        JsonSingletons.save(type);
    }

    /**
     * Reload an instance of a specific type from memory
     * @param type type to reload from memory
     */
    public static void reload(Class<? extends JsonSerializable> type) {JsonSingletons.reload(type);}
}
