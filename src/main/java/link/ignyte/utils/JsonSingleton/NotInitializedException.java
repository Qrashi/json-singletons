package link.ignyte.utils.JsonSingleton;

/**
 * Exception thrown when something has not been initialized
 */
public class NotInitializedException extends RuntimeException {
    /**
     * New NotInitializedException
     * @param s Message to display in stdout
     */
    NotInitializedException(String s) {
        super(s);
    }
}
