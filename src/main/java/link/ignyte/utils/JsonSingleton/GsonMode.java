package link.ignyte.utils.JsonSingleton;

/**
 * Gson serialisation modes
 */
public enum GsonMode {
    /**
     * Serialize all values (ignore @Expose and @SerializedName annotations)
     */
    ALL,
    /**
     * Serialize all values except for transient values (ignore @Expose and @SerializedName annotations)
     */
    ALL_EXCEPT_TRANSIENT,
    /**
     * Serialize only values with the @Expose annotation (won't ignore @SerializeName)
     */
    ONLY_EXPOSE,
}
